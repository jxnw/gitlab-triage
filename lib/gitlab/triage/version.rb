# frozen_string_literal: true

module Gitlab
  module Triage
    VERSION = '1.22.0'
  end
end

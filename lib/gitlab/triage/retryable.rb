module Gitlab
  module Triage
    module Retryable
      MAX_RETRIES = 3
      BACK_OFF_SECONDS = 10

      attr_accessor :tries

      def execute_with_retry(exception_types: [StandardError], backoff_exceptions: [])
        @tries = 0

        until maximum_retries_reached?
          begin
            @tries += 1
            return yield
          rescue *exception_types
            raise if maximum_retries_reached?
          rescue *backoff_exceptions
            raise if maximum_retries_reached?

            sleep(BACK_OFF_SECONDS)
          end
        end
      end

      private

      def maximum_retries_reached?
        tries == MAX_RETRIES
      end
    end
  end
end

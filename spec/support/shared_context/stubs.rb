RSpec.shared_context 'with stubs context' do
  let(:project_id) { 123 }
  let(:fork_project_id) { 456 }
  let(:group_id) { 23 }

  let(:admin) do
    {
      state: 'active',
      id: 1,
      web_url: 'https://gitlab.com/root',
      name: 'Administrator',
      avatar_url: nil,
      username: 'root'
    }
  end

  let(:issues) { [issue] }
  let(:issue) do
    {
      state: 'open',
      description: 'issue description',
      author: {
        state: 'active',
        id: 18,
        web_url: 'https://gitlab.com/eileen.lowe',
        name: 'Alexandra Bashirian',
        avatar_url: nil,
        username: 'eileen.lowe'
      },
      milestone: {
        project_id: project_id,
        description: 'milestone description',
        state: 'closed',
        due_date: nil,
        iid: 2,
        created_at: '2016-01-04T15:31:39.996Z',
        title: 'v4.0',
        id: 17,
        updated_at: '2016-01-04T15:31:39.996Z'
      },
      project_id: project_id,
      assignees: [admin],
      assignee: admin,
      updated_at: '2016-01-04T15:31:51.081Z',
      closed_at: nil,
      closed_by: nil,
      id: 76,
      title: 'issue title',
      created_at: '2016-01-04T15:31:51.081Z',
      iid: 6,
      labels: %w[bug],
      user_notes_count: 1,
      due_date: '2016-07-22',
      web_url: "https://gitlab.com/example/example/issue/6",
      confidential: false,
      weight: nil,
      discussion_locked: false,
      time_stats: {
        time_estimate: 0,
        total_time_spent: 0,
        human_time_estimate: nil,
        human_total_time_spent: nil
      }
    }
  end
  let(:graphql_issues) { [graphql_issue] }
  let(:graphql_issue) do
    {
      id: 76,
      iid: 1000,
      title: 'Issue Title',
      updated_at: "2021-04-15T05:46:29Z",
      created_at: "2021-04-15T05:44:27Z",
      web_url: "https://gitlab.com/test-group/test-project/-/issues/10000",
      project_id: 100001,
      user_notes_count: 15
    }
  end

  let(:mrs) { [mr] }
  let(:mr) do
    {
      id: 1,
      iid: 1,
      project_id: project_id,
      title: "test1",
      description: "fixed login page css paddings",
      state: "merged",
      merged_by: {
        id: 87854,
        name: "Douwe Maan",
        username: "DouweM",
        state: "active",
        avatar_url: "https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png",
        web_url: "https://gitlab.com/DouweM"
      },
      merged_at: "2018-09-07T11:16:17.520Z",
      closed_by: nil,
      closed_at: nil,
      created_at: "2017-04-29T08:46:00Z",
      updated_at: "2017-04-29T08:46:00Z",
      target_branch: "master",
      source_branch: "test1",
      upvotes: 0,
      downvotes: 0,
      author: {
        id: 1,
        name: "Administrator",
        username: "admin",
        state: "active",
        avatar_url: nil,
        web_url: "https://gitlab.example.com/admin"
      },
      assignee: {
        id: 1,
        name: "Administrator",
        username: "admin",
        state: "active",
        avatar_url: nil,
        web_url: "https://gitlab.example.com/admin"
      },
      assignees: [{
        name: "Miss Monserrate Beier",
        username: "axel.block",
        id: 12,
        state: "active",
        avatar_url: "http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon",
        web_url: "https://gitlab.example.com/axel.block"
      }],
      source_project_id: fork_project_id,
      target_project_id: project_id,
      labels: [
        "Community contribution",
        "Manage"
      ],
      work_in_progress: false,
      milestone: {
        id: 5,
        iid: 1,
        project_id: project_id,
        title: "v2.0",
        description: "Assumenda aut placeat expedita exercitationem labore sunt enim earum.",
        state: "closed",
        created_at: "2015-02-02T19:49:26.013Z",
        updated_at: "2015-02-02T19:49:26.013Z",
        due_date: "2018-09-22",
        start_date: "2018-08-08",
        web_url: "https://gitlab.example.com/my-group/my-project/milestones/1"
      },
      merge_when_pipeline_succeeds: true,
      merge_status: "can_be_merged",
      sha: "8888888888888888888888888888888888888888",
      merge_commit_sha: nil,
      user_notes_count: 1,
      discussion_locked: nil,
      should_remove_source_branch: true,
      force_remove_source_branch: false,
      allow_collaboration: false,
      allow_maintainer_to_push: false,
      web_url: "http://gitlab.example.com/my-group/my-project/merge_requests/1",
      time_stats: {
        time_estimate: 0,
        total_time_spent: 0,
        human_time_estimate: nil,
        human_total_time_spent: nil
      },
      squash: false,
      task_completion_status: {
        count: 0,
        completed_count: 0
      }
    }
  end

  let(:epics) { [epic] }
  let(:epic) do
    {
      id: 9,
      iid: 1,
      group_id: 23,
      parent_id: nil,
      title: 'epic title',
      description: 'epic description',
      confidential: false,
      author: {
        state: 'active',
        id: 18,
        web_url: 'https://gitlab.com/eileen.lowe',
        name: 'Alexandra Bashirian',
        avatar_url: nil,
        username: 'eileen.lowe'
      },
      start_date: nil,
      start_date_is_fixed: false,
      start_date_fixed: nil,
      start_date_from_inherited_source: nil,
      start_date_from_milestones: nil,
      end_date: nil,
      due_date: nil,
      due_date_is_fixed: false,
      due_date_fixed: nil,
      due_date_from_inherited_source: nil,
      due_date_from_milestones: nil,
      state: 'opened',
      web_url: 'https://gitlab.com/example/example/-/epics/9',
      references: {
        short: "&9",
        relative: "&9",
        full: "example/example&9"
      },
      created_at: '2021-01-07T13:46:48.348Z',
      updated_at: '2021-01-07T13:49:28.289Z',
      closed_at: nil,
      labels: [],
      upvotes: 0,
      downvotes: 0,
      _links: {
        self: 'https://gitlab.com/api/v4/groups/23/epics/9',
        epic_issues: 'https://gitlab.com/api/v4/groups/23/epics/9/issues',
        group: 'https://gitlab.com/api/v4/groups/23'
      }
    }
  end
end

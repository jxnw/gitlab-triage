RSpec.shared_examples 'issuable' do
  let(:triage_resource) { described_class.new(resource, network: network) }
  let(:full_resource_reference) { 'project/path!456' }
  let(:resource) { { project_id: 123, iid: 456, references: { full: full_resource_reference } } }

  describe 'project path functionality' do
    let(:response) do
      [
        {
          path_with_namespace: 'project/path'
        }
      ]
    end

    before do
      allow(network).to receive(:query_api_cached).with("#{base_url}/projects/#{resource[:project_id]}?").and_return(response)
    end

    describe '#full_resource_reference' do
      subject { triage_resource.full_resource_reference }

      it 'has the correct reference' do
        expect(subject).to eq(full_resource_reference)
      end
    end

    describe '#project_path' do
      subject { triage_resource.project_path }

      it 'has the correct path' do
        expect(subject).to eq(response.first[:path_with_namespace])
      end
    end
  end
end

# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/resource/issue'

describe Gitlab::Triage::Resource::Issue do
  include_context 'with network context'

  it_behaves_like 'issuable'

  describe '#url' do
    subject { described_class.new(resource, network: network).__send__(:url) }

    let(:resource) { { source_id_name => 123 } }

    context 'when source is project' do
      let(:source_id_name) { :project_id }

      it 'returns the url pointing to the current resources' do
        expect(subject).to eq("#{base_url}/projects/123/issues?per_page=100")
      end
    end

    context 'when source is group' do
      let(:source_id_name) { :group_id }

      it 'returns the url pointing to the current resources' do
        expect(subject).to eq("#{base_url}/groups/123/issues?per_page=100")
      end
    end
  end

  describe '#merge_requests_count' do
    subject { described_class.new(resource) }

    let(:resource) { { merge_requests_count: 2 } }

    it 'returns value from merge_requests_count' do
      expect(subject.merge_requests_count).to eq(2)
    end
  end

  describe '#related_merge_requests' do
    include_context 'with stubs context'

    subject { described_class.new(resource, network: network) }

    let(:resource) { { project_id: 123, iid: 1 } }

    context 'when querying loads related_merge_requests from API' do
      it 'returns the value from the API' do
        stub_get = stub_api(
          :get,
          "#{base_url}/projects/123/issues/1/related_merge_requests",
          query: { per_page: 100 }
        ) do
          [{ iid: 321 }]
        end

        related_merge_requests = subject.related_merge_requests
        expect(related_merge_requests.size).to eq(1)
        expect(related_merge_requests[0].resource[:iid]).to eq(321)
        assert_requested(stub_get)
      end
    end
  end

  describe '#closed_by' do
    include_context 'with stubs context'

    subject { described_class.new(resource, network: network) }

    let(:resource) { { project_id: 123, iid: 1 } }

    context 'when querying loads closed_by from API' do
      it 'returns the value from the API' do
        stub_get = stub_api(
          :get,
          "#{base_url}/projects/123/issues/1/closed_by",
          query: { per_page: 100 }
        ) do
          [{ iid: 321 }]
        end

        closed_merge_requests = subject.closed_by
        expect(closed_merge_requests.size).to eq(1)
        expect(closed_merge_requests[0].resource[:iid]).to eq(321)
        assert_requested(stub_get)
      end
    end
  end
end
